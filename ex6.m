if ~exist('dhexecutado') % DH apenas para comparar no final
    dh;
    dhexecutado = true;
end

close all;

config;

%
x = [1 0 0]';
y = [0 1 0]';
z = [0 0 1]';

h1 = z;
h2 = x;
h3 = x;
h4 = y;
h5 = x;
h6 = y;

H=[h1 h2 h3 h4 h5 h6];
p01 = l0*z;
p12 = l1a*y + l1b*z;
p23 = l2*z;
p34 = l3a*y + l3b*z;
p45 = l4*y;
p56 = l5*y;

P=[p01 p12 p23 p34 p45 p56];
type=[0 0 0 0 0 0]; % 6R robot
n=6;

theta = cA';
JA = jacob(theta,type,H,P,n);
JAdh = mh12.jacob0(theta);
theta = cB';
JB = jacob(theta,type,H,P,n);
JBdh = mh12.jacob0(theta);
theta = cC';
JC = jacob(theta,type,H,P,n);
JCdh = mh12.jacob0(theta);