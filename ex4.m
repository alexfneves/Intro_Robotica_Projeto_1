if ~exist('dhexecutado')
    dh;
    dhexecutado = true;
end

%%
q0 = [0 -pi/2 pi]';
mh12_3.plot(q0', 'view', [60 30], 'zoom', 1.2, 'floorlevel', -l0-0.5, 'noname', 'notiles');
export_fig('latex/figs/ex4_init_b', '-pdf', '-painters', '-transparent');
[Td, Tdall] = mh12.fkine(cB);
pd = Tdall(1:3,4,4);
open_system('ex4sim');
sim('ex4sim');
ex4_q.Data(:,:,end)
export_fig('latex/figs/ex4_end_b', '-pdf', '-painters', '-transparent');

%%
q0 = [0 pi/2 pi/3]';
mh12_3.plot(q0', 'view', [60 30], 'zoom', 1.2, 'floorlevel', -l0-0.5, 'noname', 'notiles');
export_fig('latex/figs/ex4_init_c', '-pdf', '-painters', '-transparent');
[Td, Tdall] = mh12.fkine(cC);
pd = Tdall(1:3,4,4);
open_system('ex4sim');
sim('ex4sim');
ex4_q.Data(:,:,end)
export_fig('latex/figs/ex4_end_c', '-pdf', '-painters', '-transparent');

