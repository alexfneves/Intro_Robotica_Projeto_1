if ~exist('dhexecutado')
    dh;
    dhexecutado = true;
end

%T = [rotx(pi/2) [0 0.5 0]'; 0 0 0 1];
T = mh12.fkine(cA);
%%
T(2,4) = 0.5;
T(1:3,1:3) = rotx(pi/2);
T
q = mh12.ikine(T, [0 0 0 0 pi/2 0])
mh12.plot(q, 'view', [60 30], 'zoom', 1.2, 'floorlevel', -l0-0.5, 'noname', 'notiles');
export_fig('latex/figs/ex3_a', '-pdf', '-painters', '-transparent');
%%
T(2,4) = 0.5;
T(1:3,1:3) = rotx(-pi/2);
T
q = mh12.ikine(T, [0 0 0 0 -pi/2 0])
mh12.plot(q, 'view', [60 30], 'zoom', 1.2, 'floorlevel', -l0-0.5, 'noname', 'notiles');
export_fig('latex/figs/ex3_b', '-pdf', '-painters', '-transparent');
