if ~exist('ex1executado')
    ex1;
    ex1executado = true;
end

h1 = z;
h2 = x;
h3 = x;
h4 = y;
h5 = x;
h6 = y;

R02 = R01*R12;
R03 = R02*R23;
R04 = R03*R34;
R05 = R04*R45;
R06 = R05*R56;

p66 = zeros(3,1);
p46 = p45 + R45*p56;
p36 = p34 + R34*p46;
p26 = p23 + R23*p36;
p16 = p12 + R12*p26;

J6 = [cross(h1,R01*p16) cross(R01*h2,R02*p26) cross(R02*h3,R03*p36) cross(R03*h4,R04*p46) cross(R04*h5,R05*p56) cross(R05*h6,R06*p66) ;
    R01*h1 R02*h2 R03*h3 R04*h4 R05*h5 R06*h6];
J6 = vpa(simplify(J6));


ex5_func = matlabFunction(J6);
JA = ex5_func(cA(1), cA(2), cA(3), cA(4), cA(5))
JB = ex5_func(cB(1), cB(2), cB(3), cB(4), cB(5))
JC = ex5_func(cC(1), cC(2), cC(3), cC(4), cC(5))