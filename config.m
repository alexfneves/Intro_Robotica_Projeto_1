
% parametros
l0 = 0.175;
l1a = 0.155;
l1b = 0.275;
l2 = 0.614;
l3a = 0.12;
l3b = 0.2;
l4 = 0.52;
l5 = 0.1;
l6 = 0;

% configurações do manipulador
cA = [0 0 0 0 0 0];
cB = [-pi/4 -pi/2 pi/2 pi/2 -pi/2 -pi/2];
cC = [0 pi/3 pi/3 pi/2 pi/2 pi/2];