clear;

syms t1 t2 t3 t4 t5 t6 real;

config;


x = [1 0 0]';
y = [0 1 0]';
z = [0 0 1]';


R01 = rotz(t1);
R12 = rotx(t2);
R23 = rotx(t3);
R34 = roty(t4);
R45 = rotx(t5);
R56 = roty(t6);

p01 = l0*z;
p12 = l1a*y + l1b*z;
p23 = l2*z;
p34 = l3a*y + l3b*z;
p45 = l4*y;
p56 = l5*y;
p6e = l6*y;

T01 = [R01 p01; zeros(1,3) 1];
T12 = [R12 p12; zeros(1,3) 1];
T23 = [R23 p23; zeros(1,3) 1];
T34 = [R34 p34; zeros(1,3) 1];
T45 = [R45 p45; zeros(1,3) 1];
T56 = [R56 p56; zeros(1,3) 1];
T6e = [eye(3) p6e; zeros(1,3) 1];

T0e = T01*T12*T23*T34*T45*T56*T6e;


%matlabFunction(T0e, 'File','ex1_func');
ex1_func = matlabFunction(T0e);